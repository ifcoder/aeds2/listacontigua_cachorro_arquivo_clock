# Lista Contigua (Cachorro) | Leitura de Arquivo e Clock

- Este código C++ pertence a disciplina Algoritmos e Estrutura de Dados II
- Esta é a primeira estrutura de dados que vamos estudar
- [Material de estudo](https://summer-pocket-6a4.notion.site/2-1-Lista-Cont-gua-1ee03e135d5745e9be31331c0f9dca0a) 

  
## Linguagem C++
- O estudo desta disciplina é feito usando a linguagem de programação C++

## NetBeans
- Este projeto foi desenvolvido utilizando a IDE Netbeans. 
- Para rodá-lo é necessário ter o compilador C++ em sua máquina e a IDE NetBeans configurada para a linguagem C++

## Leitura de arquivo fstream
- Este projeto faz uso da biblioteca fstream para leitura e escrita em arquivo
- Assim podemos povoar automaticamente nossa lista e fazer alguns testes interessantes
  
  
## Clock
- Também está sendo utilizado o tClock para mensurar o tempo em algumas funções
- Pode ser usado em
  - Buscas
  - leitura
  - escritas e etc